## About Seth

### My Role:
My job as an Engineering manager is to empower my team. I want to provide them the support they need so they can make great decisions and deliver a great product. 

#### Things I care about:
- Honesty - Its important to me that we talk openly and honestly about things. If you do not believe in something you are working on, have not been able to focus recently, are having issues with things that I ask, I would like to discuss these things. When we speak honestly about these things, it builds trust and makes it easier for us to achieve more.
- Bias-towards-action - Particularly in an async environment, a bias-towards-action is important. Don't be afraid to make mistakes. I don't think a manager should ever get upset when a colleague does something incorrectly or not as the manager might have wanted when the alternative was to sit around and wait. Something done incorrectly (when it’s a two-way door decision) is much better than something not done.  After-all, its an iteration! 
- Candor - I like to be candid about ideas, problems, etc. I prefer saving the cognitive load for discussing ideas and brainstorming instead of focusing on how to say things perfectly.  This can mean that sometimes things don't come across perfectly. If my candor comes across in a way that upsets you, please let's discuss. 
- Direct-communication -  I think its important that when people have issues they speak to the closest person to the problem.  I believe keeping communication as flat as possible is more efficient, can get issues resolved faster, and reduces misunderstandings.
- Closure -  I like closure on topics instead of letting a discussion fade. I think conversations should end with next steps, or a clear "there are no next steps".  If the outcome of a discussion is not to follow up or take any action on a certain topic, lets acknowledge that clearly. 

#### Things I like:

- Please counter my opinions or ideas if you disagree or have a better idea.  It is impossible for me or anyone to have all the correct answers. As a result, I am expecting my team members to disagree with me. 
- Disagree, but come to the table with solutions. 
- When you have a question, I like to answer the question and also share my 'philosophy' regarding the topic. My goal when I answer a question is to share with you how I think about the question. By sharing my thought process, I am hoping to empower you to come up with same answer I would come up with when you have your next question. That way you could ask "What would Seth do, and you will know".  If I don't provide you with details on why/how I made a decision, just ask. 

#### Things I think a lot about:
- How is my work directly connected to delivering customer value? That to me, is most important question when I think about what I and all my members of my team are working on.  All of our jobs are ultimately about creating a product that our customers can use. If I find myself involved in some task, I want to know, how does it directly or indirectly help deliver value to our customers. All of our jobs at a software company are about building value or selling value. 
- Learning new things. My goal is always to be taking a course or class at any given time. I aim to read one non-fiction book a month.  
- How to think about problems. Logic, mental models, fallacies, consilience, biases, etc. 

#### Things I've learned:
- Great software doesn't sell itself. Sales and marketing are really valuable. Engineers have jobs not because they make great software, but because they make great software that our sales and marketing team can sell.  There is rarely a better use of an engineer's time, than to help close a deal or better understand how customers are using our software. 


#### Terms and Conditions
* This document is not exhaustive documentation of Seth Berger and subject to change without notice. 
